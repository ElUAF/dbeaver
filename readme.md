# Quick start
**Prerequisites**

* installed JDK 11
* have MySQL database (not required for service startup)

**Instructions**

* open terminal
* ```
  ./mvnw clean package 
  ```
* ```
  ./mvnw spring-boot:run
  ```
* open web browser http://localhost:8080/swagger-ui/index.html#/

 # Run tests
**Prerequisites**

* installed JDK 11
  
Tests are run with fake (mocked) DBs

```
./mvnw clean test
```
 
 # Run integration tests
 
 **Prerequisites**
 
 * installed JDK 11
 * installed Docker
 
 Note: Integration tests are run with real dockerized DBs
 
 ```
./mvnw clean verify -Dspring.profiles.active=it
```


# Tasks
Note: run app locally and check swagger

## Task 1
check DbConnectionController


### saving
http://localhost:8080/swagger-ui/index.html#/db-connection-controller/createDatabaseConnectionUsingPOST 


### updating
http://localhost:8080/swagger-ui/index.html#/db-connection-controller/updateDatabaseConnectionUsingPUT


### listing
http://localhost:8080/swagger-ui/index.html#/db-connection-controller/getAllDatabaseConnectionsUsingGET


### deleting
http://localhost:8080/swagger-ui/index.html#/db-connection-controller/deleteDatabaseConnectionUsingDELETE


## Task 2
check DatabaseInfoController

### Listing schemas
http://localhost:8080/swagger-ui/index.html#/database-info-controller/getDatabaseSchemesUsingGET

### Listing tables
http://localhost:8080/swagger-ui/index.html#/database-info-controller/getDatabaseTablesUsingGET

### Listing columns
http://localhost:8080/swagger-ui/index.html#/database-info-controller/getTableColumnsUsingGET

### Data preview of the table
http://localhost:8080/swagger-ui/index.html#/database-info-controller/findTableDataUsingGET

## Bonus tasks

### Single endpoint for statistics about each column: min, max, avg, median value of the column.
http://localhost:8080/swagger-ui/index.html#/database-info-controller/getTableColumnsStatisticsUsingGET

### Single endpoint for statistics about each table: number of records, number of attributes.
http://localhost:8080/swagger-ui/index.html#/database-info-controller/getTableStatisticsUsingGET

### Document this REST API
http://localhost:8080/swagger-ui/index.html 