package com.ataccama.dbeaver.service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DBeaverServiceApplication

fun main(args: Array<String>) {
    runApplication<DBeaverServiceApplication>(*args)
}
