package com.ataccama.dbeaver.service.dto

data class ColumnMetadataDto(
        val name: String,
        val type: String,
        val nullable: Boolean,
        val key: String,
        val defaultValue: String?,
        val extra: String
)