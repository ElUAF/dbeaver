package com.ataccama.dbeaver.service.dto

data class ColumnStatisticsDto(
        val min: Double?,
        val max: Double?,
        val avg: Double?,
        val median: Double?
)
