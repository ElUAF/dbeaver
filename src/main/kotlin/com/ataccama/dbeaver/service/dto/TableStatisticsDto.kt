package com.ataccama.dbeaver.service.dto

data class TableStatisticsDto(
        val recordCount: Long,
        val columnsCount: Int
)
