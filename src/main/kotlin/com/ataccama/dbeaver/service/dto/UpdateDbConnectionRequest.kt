package com.ataccama.dbeaver.service.dto

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class UpdateDbConnectionRequest(

        @get:Size(max = 250)
        @get:NotBlank
        val name: String,

        @get:Size(max = 250)
        @get:NotBlank
        val hostName: String,

        @get:Min(0)
        @get:Max(65535)
        val port: Int,

        @get:Size(max = 250)
        @get:NotBlank
        val databaseName: String,

        @get:Size(max = 250)
        @get:NotBlank
        val username: String,

        @get:Size(max = 250)
        @get:NotBlank
        val password: String
)
