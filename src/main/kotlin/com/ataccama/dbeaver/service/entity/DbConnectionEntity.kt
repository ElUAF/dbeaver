package com.ataccama.dbeaver.service.entity

import org.springframework.data.annotation.Id

typealias DbConnectionId = Long

data class DbConnectionEntity(

        @Id
        val id: DbConnectionId,
        val name: String,
        val hostName: String,
        val port: Int,
        val databaseName: String,
        val username: String,
        val password: String
)