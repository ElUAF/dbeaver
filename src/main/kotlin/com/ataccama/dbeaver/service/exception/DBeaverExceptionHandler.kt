package com.ataccama.dbeaver.service.exception

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

val LOGGER: Logger = LoggerFactory.getLogger(DBeaverExceptionHandler::class.java)

@ControllerAdvice
class DBeaverExceptionHandler {

    @ExceptionHandler(value = [Exception::class])
    protected fun handleException(ex: Exception): ResponseEntity<Any?>? {
        // this app doesn't relay on exceptions so that if any exception goes here something is terribly wrong ;-)
        LOGGER.error("unchecked exception is thrown!", ex)
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
    }
}