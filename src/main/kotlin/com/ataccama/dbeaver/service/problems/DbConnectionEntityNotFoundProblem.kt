package com.ataccama.dbeaver.service.problems

import com.ataccama.dbeaver.service.entity.DbConnectionId
import org.springframework.http.HttpStatus

data class DbConnectionEntityNotFoundProblem(
        val id: DbConnectionId
) : Problem {

    override val type = "DbConnectionEntity/NotFound"

    override val status: HttpStatus = HttpStatus.NOT_FOUND
}