package com.ataccama.dbeaver.service.problems

import com.ataccama.dbeaver.service.entity.DbConnectionId
import org.springframework.http.HttpStatus

data class DbOperationConnectionFailedProblem(
        val id: DbConnectionId
) : Problem {

    override val type = "DbOperation/ConnectionFailed"

    override val status: HttpStatus = HttpStatus.SERVICE_UNAVAILABLE
}