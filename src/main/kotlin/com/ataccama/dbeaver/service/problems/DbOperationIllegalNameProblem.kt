package com.ataccama.dbeaver.service.problems

import org.springframework.http.HttpStatus

data class DbOperationIllegalNameProblem(
        val illegalNames: List<String>
) : Problem {

    override val type: String = "DbOperation/IllegalName"

    override val status: HttpStatus = HttpStatus.BAD_REQUEST
}