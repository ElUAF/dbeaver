package com.ataccama.dbeaver.service.problems

import org.springframework.http.HttpStatus

data class DbOperationSqlProblem(
        val message: String
) : Problem {

    override val type = "DbOperation/SqlProblem"

    override val status = HttpStatus.INTERNAL_SERVER_ERROR
}