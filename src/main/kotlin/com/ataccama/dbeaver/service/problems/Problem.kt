package com.ataccama.dbeaver.service.problems

import org.springframework.http.HttpStatus

interface Problem {

    val status: HttpStatus

    val type: String
}