package com.ataccama.dbeaver.service.problems

import org.springframework.http.HttpStatus

data class Error(val message: String)

data class ValidationProblem(
        val fields: Map<String, List<Error>>
) : Problem {

    override val type: String = "ValidationProblem"

    override val status: HttpStatus = HttpStatus.BAD_REQUEST
}