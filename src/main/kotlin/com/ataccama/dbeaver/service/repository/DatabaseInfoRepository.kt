package com.ataccama.dbeaver.service.repository

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.problems.Problem

interface DatabaseInfoRepository {

    fun findSchemas(): Either<Problem, List<String>>

    fun findTables(): Either<Problem, List<String>>

    fun findTableColumns(tableName: String): Either<Problem, List<ColumnMetadataDto>>

    fun findTableData(tableName: String): Either<Problem, List<Map<String, String?>>>

    fun findTableColumnsStatistics(tableName: String, columnName: String): Either<Problem, ColumnStatisticsDto>

    fun findTableStatistics(tableName: String): Either<Problem, TableStatisticsDto>
}