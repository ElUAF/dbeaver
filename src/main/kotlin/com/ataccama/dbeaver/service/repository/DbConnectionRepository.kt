package com.ataccama.dbeaver.service.repository

import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.entity.DbConnectionId
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DbConnectionRepository : CrudRepository<DbConnectionEntity, DbConnectionId>