package com.ataccama.dbeaver.service.repository

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.problems.DbOperationIllegalNameProblem
import com.ataccama.dbeaver.service.problems.DbOperationSqlProblem
import com.ataccama.dbeaver.service.problems.Problem
import org.slf4j.LoggerFactory
import org.springframework.data.repository.NoRepositoryBean
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.util.stream.IntStream

private val LOGGER = LoggerFactory.getLogger(MySqlDatabaseInfoRepository::class.java)
private val LEGAL_NAME_REGEXP = Regex("^[a-zA-Z0-9_$]+$")

@NoRepositoryBean
class MySqlDatabaseInfoRepository(private val connection: Connection) : DatabaseInfoRepository {

    override fun findSchemas(): Either<Problem, List<String>> = getResults("show schemas") {
        it.getString(1)
    }

    override fun findTables(): Either<Problem, List<String>> = getResults("show tables") {
        it.getString(1)
    }

    override fun findTableColumns(tableName: String): Either<Problem, List<ColumnMetadataDto>> = getResults(
            checkNames = listOf(tableName),
            query = "SHOW COLUMNS FROM $tableName"
    ) {
        ColumnMetadataDto(
                name = it.getString(1),
                type = it.getString(2),
                nullable = it.getString(3).toLowerCase() != "no",
                key = it.getString(4),
                defaultValue = it.getString(5),
                extra = it.getString(6)
        )
    }

    override fun findTableColumnsStatistics(tableName: String, columnName: String): Either<Problem, ColumnStatisticsDto> = getResults(
            checkNames = listOf(tableName, columnName),
            query = """
                SELECT 
                max(t.$columnName) max, 
                min(t.$columnName) min, 
                avg(t.$columnName) avg,
                SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.$columnName ORDER BY t.$columnName), ',', CEILING((COUNT(t.$columnName)/2))), ',', -1) median
                 from $tableName t
            """
    ) {
        ColumnStatisticsDto(
                max = it.getDouble(1),
                min = it.getDouble(2),
                avg = it.getDouble(3),
                median = it.getDouble(4)
        )
    }.map {
        it.firstOrNull() ?: ColumnStatisticsDto(
                max = null,
                min = null,
                avg = null,
                median = null
        )
    }

    override fun findTableData(tableName: String): Either<Problem, List<Map<String, String?>>> = getResults(
            checkNames = listOf(tableName),
            query = "SELECT * FROM $tableName"
    ) { resultSet ->
        val result: MutableMap<String, String?> = mutableMapOf()

        IntStream.rangeClosed(1, resultSet.metaData.columnCount).forEach { index ->
            result[resultSet.metaData.getColumnName(index)] = resultSet.getString(index)
        }

        result
    }

    override fun findTableStatistics(tableName: String): Either<Problem, TableStatisticsDto> = getResults(
            checkNames = listOf(tableName),
            query = """
                SELECT 
                    (SELECT COUNT(*) from $tableName) record_count, 
                    (SELECT COUNT(*)
                        FROM information_schema.columns
                        WHERE table_schema = database()
                        AND table_name = ?) columns_count
            """,
            params = listOf(tableName)
    ) { resultSet ->
        TableStatisticsDto(
                recordCount = resultSet.getLong(1),
                columnsCount = resultSet.getInt(2)
        )
    }.map {
        it.first()
    }

    private inline fun <reified K> getResults(query: String, checkNames: List<String> = listOf(), params: List<Any> = listOf(), mapper: (resultSet: ResultSet) -> K): Either<Problem, List<K>> {
        val illegalNames = checkNames.filter { !it.matches(LEGAL_NAME_REGEXP) }

        if (illegalNames.isNotEmpty()) {
            return Either.left(DbOperationIllegalNameProblem(illegalNames))
        }

        val statement = connection.prepareStatement(query)

        return statement.use {
            try {
                IntStream.range(0, params.size).forEach { index -> it.setObject(index + 1, params[index]) }

                val resultSet = it.executeQuery()
                val result = mutableListOf<K>()

                while (resultSet.next()) {
                    result.add(mapper(resultSet))
                }

                Either.right(result)
            } catch (ex: SQLException) {
                LOGGER.info("SQL query failed $query", ex)
                Either.left(DbOperationSqlProblem(ex.message ?: "Unknown SQL error"))
            }
        }
    }
}