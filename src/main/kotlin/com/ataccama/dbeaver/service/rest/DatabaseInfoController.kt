package com.ataccama.dbeaver.service.rest

import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.service.DatabaseInfoService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/dbs/{id}/", produces = [MediaType.APPLICATION_JSON_VALUE])
class DatabaseInfoController(
        val databaseInfoService: DatabaseInfoService,
        val responseHandler: ResponseHandler
) {

    @GetMapping("schemes")
    fun getDatabaseSchemes(@PathVariable("id") id: DbConnectionId): ResponseEntity<List<String>> = databaseInfoService.findSchemas(id)
            .let(responseHandler::handleResult)


    @GetMapping("tables")
    fun getDatabaseTables(@PathVariable("id") id: DbConnectionId): ResponseEntity<List<String>> = databaseInfoService.findTables(id)
            .let(responseHandler::handleResult)

    @GetMapping("tables/{tableName}/statistics")
    fun getTableStatistics(@PathVariable("id") id: DbConnectionId,
                           @PathVariable("tableName") tableName: String
    ): ResponseEntity<TableStatisticsDto> = databaseInfoService.findTableStatistics(id, tableName)
            .let(responseHandler::handleResult)

    @GetMapping("tables/{tableName}/columns")
    fun getTableColumns(@PathVariable("id") id: DbConnectionId,
                        @PathVariable("tableName") tableName: String
    ): ResponseEntity<List<ColumnMetadataDto>> = databaseInfoService.findTableColumns(id, tableName)
            .let(responseHandler::handleResult)

    @GetMapping("tables/{tableName}/columns/{columnName}/statistics")
    fun getTableColumnsStatistics(@PathVariable("id") id: DbConnectionId,
                                  @PathVariable("tableName") tableName: String,
                                  @PathVariable("columnName") columnName: String
    ): ResponseEntity<ColumnStatisticsDto> = databaseInfoService.findTableColumnsStatistics(id, tableName, columnName)
            .let(responseHandler::handleResult)

    @GetMapping("tables/{tableName}/data")
    fun findTableData(@PathVariable("id") id: DbConnectionId,
                      @PathVariable("tableName") tableName: String
    ): ResponseEntity<List<Map<String, String?>>> = databaseInfoService.findTableData(id, tableName)
            .let(responseHandler::handleResult)
}
