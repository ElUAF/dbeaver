package com.ataccama.dbeaver.service.rest

import com.ataccama.dbeaver.service.dto.CreateDbConnectionRequest
import com.ataccama.dbeaver.service.dto.UpdateDbConnectionRequest
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.service.DbConnectionService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/dbs", produces = [MediaType.APPLICATION_JSON_VALUE])
class DbConnectionController(
        val dbConnectionService: DbConnectionService,
        val responseHandler: ResponseHandler
) {

    @GetMapping
    fun getAllDatabaseConnections(): ResponseEntity<List<DbConnectionEntity>> = dbConnectionService.findAll()
            .let(responseHandler::handleResult)

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createDatabaseConnection(
            @RequestBody createDbConnectionRequest: CreateDbConnectionRequest
    ): ResponseEntity<DbConnectionEntity> = dbConnectionService.create(createDbConnectionRequest)
            .let(responseHandler::handleResult)

    @PutMapping("{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateDatabaseConnection(@PathVariable("id") id: DbConnectionId,
                                 @RequestBody updateDbConnectionRequest: UpdateDbConnectionRequest
    ): ResponseEntity<DbConnectionEntity> = dbConnectionService.update(id, updateDbConnectionRequest)
            .let(responseHandler::handleResult)

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteDatabaseConnection(@PathVariable("id") id: DbConnectionId): ResponseEntity<Unit> = dbConnectionService.deleteById(id)
            .let(responseHandler::handleResult)
}