package com.ataccama.dbeaver.service.rest

import arrow.core.Either
import com.ataccama.dbeaver.service.problems.Problem
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component
class ResponseHandler {

    fun <K> handleResult(result: Either<Problem, K>): ResponseEntity<K> = result.fold(
            { ResponseEntity.status(it.status).body(it) as ResponseEntity<K> },
            { ResponseEntity.ok(it) }
    )
}