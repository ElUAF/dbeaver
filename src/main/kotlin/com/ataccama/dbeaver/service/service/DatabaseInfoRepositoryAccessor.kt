package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.repository.DatabaseInfoRepository

interface DatabaseInfoRepositoryAccessor {

    fun <K> useRepository(dbConnection: DbConnectionEntity, operation: (repository: DatabaseInfoRepository) -> Either<Problem, K>): Either<Problem, K>
}