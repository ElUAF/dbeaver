package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.repository.DatabaseInfoRepository
import com.ataccama.dbeaver.service.repository.MySqlDatabaseInfoRepository
import org.springframework.stereotype.Component
import java.sql.DriverManager


@Component
class DatabaseInfoRepositoryAccessorImpl : DatabaseInfoRepositoryAccessor {

    override fun <K> useRepository(dbConnection: DbConnectionEntity, operation: (repository: DatabaseInfoRepository) -> Either<Problem, K>): Either<Problem, K> {
        Class.forName("com.mysql.cj.jdbc.Driver")

        val connection = DriverManager.getConnection("jdbc:mysql://${dbConnection.hostName}:${dbConnection.port}/${dbConnection.databaseName}", dbConnection.username, dbConnection.password)

        return connection.use {
            it.autoCommit = false
            val operationResult = operation(MySqlDatabaseInfoRepository(it))
            it.rollback()
            operationResult
        }
    }
}