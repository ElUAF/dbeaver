package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.problems.Problem

interface DatabaseInfoService {

    fun findSchemas(id: DbConnectionId): Either<Problem, List<String>>

    fun findTables(id: DbConnectionId): Either<Problem, List<String>>

    fun findTableColumns(id: DbConnectionId, tableName: String): Either<Problem, List<ColumnMetadataDto>>

    fun findTableColumnsStatistics(id: DbConnectionId, tableName: String, columnName: String): Either<Problem, ColumnStatisticsDto>

    fun findTableData(id: DbConnectionId, tableName: String): Either<Problem, List<Map<String, String?>>>

    fun findTableStatistics(id: DbConnectionId, tableName: String): Either<Problem, TableStatisticsDto>
}