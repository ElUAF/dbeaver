package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.problems.DbConnectionEntityNotFoundProblem
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.repository.DatabaseInfoRepository
import com.ataccama.dbeaver.service.repository.DbConnectionRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class DatabaseInfoServiceImpl(
        val dbConnectionRepository: DbConnectionRepository,
        val databaseInfoRepositoryAccessor: DatabaseInfoRepositoryAccessor
) : DatabaseInfoService {

    override fun findSchemas(id: DbConnectionId): Either<Problem, List<String>> = useDbConnectionId(id) {
        it.findSchemas()
    }

    override fun findTables(id: DbConnectionId): Either<Problem, List<String>> = useDbConnectionId(id) {
        it.findTables()
    }

    override fun findTableColumns(id: DbConnectionId, tableName: String): Either<Problem, List<ColumnMetadataDto>> = useDbConnectionId(id) {
        it.findTableColumns(tableName)
    }

    override fun findTableColumnsStatistics(id: DbConnectionId, tableName: String, columnName: String): Either<Problem, ColumnStatisticsDto> = useDbConnectionId(id) {
        it.findTableColumnsStatistics(tableName, columnName)
    }

    override fun findTableData(id: DbConnectionId, tableName: String): Either<Problem, List<Map<String, String?>>> = useDbConnectionId(id) {
        it.findTableData(tableName)
    }

    override fun findTableStatistics(id: DbConnectionId, tableName: String): Either<Problem, TableStatisticsDto> = useDbConnectionId(id) {
        it.findTableStatistics(tableName)
    }

    fun <K : Any> useDbConnectionId(id: DbConnectionId, operation: (repository: DatabaseInfoRepository) -> Either<Problem, K>): Either<Problem, K> = dbConnectionRepository
            .findById(id)
            .map { dbConnection ->
                databaseInfoRepositoryAccessor.useRepository(dbConnection) {
                    operation(it)
                }
            }
            .orElse(Either.left(DbConnectionEntityNotFoundProblem(id)))
}