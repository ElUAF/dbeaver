package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.CreateDbConnectionRequest
import com.ataccama.dbeaver.service.dto.UpdateDbConnectionRequest
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.problems.Problem

interface DbConnectionService {

    fun create(request: CreateDbConnectionRequest): Either<Problem, DbConnectionEntity>

    fun update(id: DbConnectionId, request: UpdateDbConnectionRequest): Either<Problem, DbConnectionEntity>

    fun findAll(): Either<Problem, List<DbConnectionEntity>>

    fun deleteById(id: DbConnectionId): Either<Problem, Unit>
}
