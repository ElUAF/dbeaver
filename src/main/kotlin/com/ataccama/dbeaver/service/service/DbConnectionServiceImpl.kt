package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.CreateDbConnectionRequest
import com.ataccama.dbeaver.service.dto.UpdateDbConnectionRequest
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.entity.DbConnectionId
import com.ataccama.dbeaver.service.problems.DbConnectionEntityNotFoundProblem
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.repository.DbConnectionRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class DbConnectionServiceImpl(
        val dbConnectionRepository: DbConnectionRepository,
        val validationService: ValidationService
) : DbConnectionService {

    @Transactional
    override fun create(request: CreateDbConnectionRequest): Either<Problem, DbConnectionEntity> = validationService.withValidation(request) {
        dbConnectionRepository.save(DbConnectionEntity(
                id = 0,
                name = request.name,
                hostName = request.hostName,
                port = request.port,
                databaseName = request.databaseName,
                username = request.username,
                password = request.password
        )).let { Either.right(it) }
    }

    @Transactional
    override fun update(id: DbConnectionId, request: UpdateDbConnectionRequest): Either<Problem, DbConnectionEntity> = validationService.withValidation(request) {
        val storedEntity = dbConnectionRepository.findById(id).orElse(null) ?: return@withValidation Either.left(DbConnectionEntityNotFoundProblem(id))

        dbConnectionRepository.save(storedEntity.copy(
                name = request.name,
                hostName = request.hostName,
                port = request.port,
                databaseName = request.databaseName,
                username = request.username,
                password = request.password
        )).let { Either.right(it) }
    }

    override fun findAll(): Either<Problem, List<DbConnectionEntity>> = dbConnectionRepository.findAll()
            .toList()
            .let { Either.right(it) }

    @Transactional
    override fun deleteById(id: DbConnectionId): Either<Problem, Unit> = dbConnectionRepository.deleteById(id)
            .let { Either.right(it) }
}
