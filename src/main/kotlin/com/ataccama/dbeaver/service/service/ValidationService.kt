package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.problems.Problem


interface ValidationService {

    fun <INPUT, OUTPUT> withValidation(validatedObject: INPUT, mapper: (validObject: INPUT) -> Either<Problem, OUTPUT>): Either<Problem, OUTPUT>
}