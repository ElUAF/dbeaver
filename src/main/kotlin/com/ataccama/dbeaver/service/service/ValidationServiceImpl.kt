package com.ataccama.dbeaver.service.service

import arrow.core.Either
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.problems.ValidationProblem
import org.springframework.stereotype.Component
import javax.validation.Validator

@Component
class ValidationServiceImpl(
        val validator: Validator
) : ValidationService {

    override fun <INPUT, OUTPUT> withValidation(validatedObject: INPUT, mapper: (validObject: INPUT) -> Either<Problem, OUTPUT>): Either<Problem, OUTPUT> {
        val constraintViolations = validator.validate(validatedObject)

        return if (constraintViolations.isNotEmpty()) {
            // it would be handy to return fields + messages from constrain violations
            Either.left(ValidationProblem(fields = mapOf()))
        } else {
            mapper.invoke(validatedObject)
        }
    }
}