package com.ataccama.dbeaver.service

import arrow.core.Either
import arrow.core.getOrElse
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.CreateDbConnectionRequest
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.dto.UpdateDbConnectionRequest
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.problems.DbConnectionEntityNotFoundProblem
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.problems.ValidationProblem
import com.ataccama.dbeaver.service.repository.DbConnectionRepository
import com.ataccama.dbeaver.service.service.DatabaseInfoService
import com.ataccama.dbeaver.service.service.DbConnectionService
import org.junit.Assert
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils
import java.util.stream.Collectors

@SpringBootTest(classes = [TestConfig::class])
@Transactional
@Configuration
abstract class DBeaverTestBase {

    @Autowired
    private lateinit var dbConnectionService: DbConnectionService

    @Autowired
    private lateinit var databaseInfoService: DatabaseInfoService

    @Autowired
    private lateinit var dbConnectionRepository: DbConnectionRepository

    @Autowired
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    @Autowired
    private lateinit var databaseTestSupport: DatabaseTestSupport

    @BeforeEach
    fun setup() {
        applicationEventPublisher.publishEvent(InitTestEvent())
    }

    @AfterEach
    fun tierDown() {
        applicationEventPublisher.publishEvent(TearDownTestEvent())
    }

    fun `given there is valid database connection`(
            name: String? = null,
            hostName: String? = null,
            port: Int? = null,
            databaseName: String? = null,
            username: String? = null,
            password: String? = null
    ): DbConnectionEntity = dbConnectionRepository.save(DbConnectionEntity(
            id = 0,
            name = name ?: DEFAULT_DB_CONNECTION_NAME,
            hostName = hostName ?: DEFAULT_DB_CONNECTION_HOSTNAME,
            port = port ?: DEFAULT_DB_CONNECTION_PORT,
            databaseName = databaseName ?: DEFAULT_DB_CONNECTION_DATABASE_NAME,
            username = username ?: DEFAULT_DB_CONNECTION_USERNAME,
            password = password ?: DEFAULT_DB_CONNECTION_PASSWORD
    ))

    fun `given there is valid database connection`(connectionInfo: ConnectionInfo): DbConnectionEntity = `given there is valid database connection`(
            name = RandomStringUtils.randomAlphanumeric(10),
            hostName = connectionInfo.hostName,
            port = connectionInfo.port,
            databaseName = connectionInfo.databaseName,
            username = connectionInfo.username,
            password = connectionInfo.password
    )

    fun `given there is valid database up and running`() = databaseTestSupport.createDatabase()

    fun `given there is valid database up, running and stored`(): DbConnectionEntity {
        val connectionInfo = `given there is valid database up and running`()
        return `given there is valid database connection`(connectionInfo)
    }

    fun `given there is schema`(dbConnection: DbConnectionEntity, schema: String) {
        databaseTestSupport.createSchema(dbConnection.toConnectionInfo(), schema)
    }

    fun `given there is table`(dbConnection: DbConnectionEntity, tableName: String, columns: List<ColumnDefinition>) {
        databaseTestSupport.createTable(dbConnection.toConnectionInfo(), tableName, columns)
    }

    fun `when load database schemes`(dbConnection: DbConnectionEntity) = databaseInfoService.findSchemas(dbConnection.id)

    fun `when load database tables`(dbConnection: DbConnectionEntity) = databaseInfoService.findTables(dbConnection.id)

    fun `when valid MySQL database connection is created`(
            name: String? = null,
            hostname: String? = null,
            port: Int? = null,
            databaseName: String? = null,
            username: String? = null,
            password: String? = null
    ): DbConnectionEntity = dbConnectionService.create(CreateDbConnectionRequest(
            name = name ?: DEFAULT_DB_CONNECTION_NAME,
            hostName = hostname ?: DEFAULT_DB_CONNECTION_HOSTNAME,
            port = port ?: DEFAULT_DB_CONNECTION_PORT,
            databaseName = databaseName ?: DEFAULT_DB_CONNECTION_DATABASE_NAME,
            username = username ?: DEFAULT_DB_CONNECTION_USERNAME,
            password = password ?: DEFAULT_DB_CONNECTION_PASSWORD
    )).fold({ throw IllegalStateException("problem should be thrown but was: $it") }, { it })

    fun `when MySQL database connection is created`(dbConnection: DbConnectionEntity): Either<Problem, DbConnectionEntity> = dbConnectionService.create(CreateDbConnectionRequest(
            name = dbConnection.name,
            hostName = dbConnection.hostName,
            port = dbConnection.port,
            databaseName = dbConnection.databaseName,
            username = dbConnection.username,
            password = dbConnection.password
    ))

    fun `when database connection is updated`(
            entity: DbConnectionEntity,
            newEntityState: DbConnectionEntity
    ) = dbConnectionService.update(entity.id, UpdateDbConnectionRequest(
            name = newEntityState.name,
            hostName = newEntityState.hostName,
            port = newEntityState.port,
            databaseName = newEntityState.databaseName,
            username = newEntityState.username,
            password = newEntityState.password
    ))

    fun `when database connection is updated`(
            entity: DbConnectionEntity,
            name: String? = null,
            hostname: String? = null,
            port: Int? = null,
            databaseName: String? = null,
            username: String? = null,
            password: String? = null
    ) = dbConnectionService.update(entity.id, UpdateDbConnectionRequest(
            name = name ?: DEFAULT_DB_CONNECTION_NAME,
            hostName = hostname ?: DEFAULT_DB_CONNECTION_HOSTNAME,
            port = port ?: DEFAULT_DB_CONNECTION_PORT,
            databaseName = databaseName ?: DEFAULT_DB_CONNECTION_DATABASE_NAME,
            username = username ?: DEFAULT_DB_CONNECTION_USERNAME,
            password = password ?: DEFAULT_DB_CONNECTION_PASSWORD
    ))

    fun `when load all database connections`() = dbConnectionService.findAll()

    fun `when database connection is deleted`(dbConnection: DbConnectionEntity) = dbConnectionService.deleteById(dbConnection.id)

    fun getExpectedEntity(entity: DbConnectionEntity,
                          name: String? = null,
                          hostname: String? = null,
                          port: Int? = null,
                          databaseName: String? = null,
                          username: String? = null,
                          password: String? = null) = entity.copy(
            name = name ?: DEFAULT_DB_CONNECTION_NAME,
            hostName = hostname ?: DEFAULT_DB_CONNECTION_HOSTNAME,
            port = port ?: DEFAULT_DB_CONNECTION_PORT,
            databaseName = databaseName ?: DEFAULT_DB_CONNECTION_DATABASE_NAME,
            username = username ?: DEFAULT_DB_CONNECTION_USERNAME,
            password = password ?: DEFAULT_DB_CONNECTION_PASSWORD
    )

    fun `then there is database connections`(expected: List<DbConnectionEntity>) = Assertions.assertEquals(expected, dbConnectionRepository.findAll().toList())

    fun `then db connection is not found`(result: Either<Problem, DbConnectionEntity>) = result.fold(
            { Assertions.assertTrue(it is DbConnectionEntityNotFoundProblem, "problem should be DbConnectionEntityNotFoundProblem but was: $it") },
            { Assertions.fail("problem should be DbConnectionEntityNotFoundProblem but was success: $it") }
    )

    fun `then schemes should be default and rest is equals to`(dbConnection: DbConnectionEntity, expectedSchemes: List<String>) {
        val result = databaseInfoService.findSchemas(dbConnection.id)

        val expected = expectedSchemes.toMutableList()
        expected.add("information_schema")
        expected.add(dbConnection.databaseName)
        expected.sort()

        Assertions.assertTrue(result.isRight(), "expected schemes $expectedSchemes but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                expected,
                result.map { it.stream().sorted().collect(Collectors.toList()) }.getOrElse { null }
        )
    }

    fun `then tables should be`(dbConnection: DbConnectionEntity, expectedSchemes: List<String>) {
        val result = databaseInfoService.findTables(dbConnection.id)
        val expected = expectedSchemes.toMutableList()
        expected.sort()

        Assertions.assertTrue(result.isRight(), "expected tables $expectedSchemes but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                expected,
                result.map { it.stream().sorted().collect(Collectors.toList()) }.getOrElse { null }
        )
    }

    fun `then table should have metadata`(dbConnection: DbConnectionEntity, tableName: String, columnDefinition: List<ColumnDefinition>) {
        val expected = columnDefinition.map { it.toMetadataDto() }.sortedBy { it.name }

        val result = databaseInfoService.findTableColumns(dbConnection.id, tableName)

        Assertions.assertTrue(result.isRight(), "expected column definitions $columnDefinition but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                expected,
                result.map { metadata -> metadata.sortedBy { it.name } }.getOrElse { null }
        )
    }

    protected fun `given table has following data`(dbConnection: DbConnectionEntity, tableName: String, data: List<Map<String, String?>>) {
        databaseTestSupport.insertData(dbConnection.toConnectionInfo(), tableName, data)
    }

    protected fun `then table should have data`(dbConnection: DbConnectionEntity, tableName: String, tableData: List<Map<String, String?>>) {
        val result = databaseInfoService.findTableData(dbConnection.id, tableName)

        Assertions.assertTrue(result.isRight(), "expected table data $tableData but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                tableData,
                result.getOrElse { null }
        )
    }

    protected fun `then column should have expected statistics`(dbConnection: DbConnectionEntity, tableName: String, columnName: String, expectedStatistics: ColumnStatisticsDto) {
        val result = databaseInfoService.findTableColumnsStatistics(dbConnection.id, tableName, columnName)

        Assertions.assertTrue(result.isRight(), "expected table [$tableName] column [$columnName] statistics but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                expectedStatistics,
                result.getOrElse { null }
        )
    }

    fun `then table should have expected statistics`(dbConnection: DbConnectionEntity, tableName: String, expectedStatistics: TableStatisticsDto) {
        val result = databaseInfoService.findTableStatistics(dbConnection.id, tableName)

        Assertions.assertTrue(result.isRight(), "expected table [$tableName] statistics but found error ${result.fold({ it }, { it })}")
        Assertions.assertEquals(
                expectedStatistics,
                result.getOrElse { null }
        )
    }

    fun <K> `then expect validation problem`(result: Either<Problem, K>) {
        result.fold(
                { Assert.assertTrue("expected validation problem but was $it", it is ValidationProblem) },
                { Assert.fail("result should be validation problem but it was success $it") }
        )
    }

    companion object {
        const val DEFAULT_DB_CONNECTION_NAME = "dbConnectionName"
        const val DEFAULT_DB_CONNECTION_HOSTNAME = "dbConnectionHostName"
        const val DEFAULT_DB_CONNECTION_PORT = 1234
        const val DEFAULT_DB_CONNECTION_DATABASE_NAME = "dbConnectionHostDatabaseName"
        const val DEFAULT_DB_CONNECTION_USERNAME = "dbConnectionHostUsername"
        const val DEFAULT_DB_CONNECTION_PASSWORD = "dbConnectionHostPassword"

        val DEFAULT_DB_CONNECTION = DbConnectionEntity(
                id = 0,
                name = DEFAULT_DB_CONNECTION_NAME,
                hostName = DEFAULT_DB_CONNECTION_HOSTNAME,
                port = DEFAULT_DB_CONNECTION_PORT,
                databaseName = DEFAULT_DB_CONNECTION_DATABASE_NAME,
                username = DEFAULT_DB_CONNECTION_USERNAME,
                password = DEFAULT_DB_CONNECTION_PASSWORD
        )
    }
}