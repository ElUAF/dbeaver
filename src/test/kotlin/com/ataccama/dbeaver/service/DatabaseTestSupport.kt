package com.ataccama.dbeaver.service

import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.entity.DbConnectionEntity

data class ConnectionInfo(
        val hostName: String,
        val port: Int,
        val databaseName: String,
        val username: String,
        val password: String
)

fun DbConnectionEntity.toConnectionInfo() = ConnectionInfo(
        hostName = hostName,
        port = port,
        databaseName = databaseName,
        username = username,
        password = password
)

data class ColumnDefinition(
        val name: String,
        val type: String,
        val nullable: Boolean = true,
        val primaryKey: Boolean = false
) {
    fun toSql(): String = "$name $type${optionalFragment(!nullable, "NOT NULL")}${optionalFragment(primaryKey, "PRIMARY KEY")}"

    private fun optionalFragment(condition: Boolean, fragment: String) = if (condition) " $fragment" else ""

    fun toMetadataDto() = ColumnMetadataDto(
            name = name,
            type = type,
            key = if (primaryKey) "PRI" else "",
            nullable = nullable,
            defaultValue = null,
            extra = ""
    )
}

interface DatabaseTestSupport {

    fun createDatabase(): ConnectionInfo

    fun createSchema(connectionInfo: ConnectionInfo, schema: String)

    fun createTable(connectionInfo: ConnectionInfo, tableName: String, columns: List<ColumnDefinition>)

    fun insertData(connectionInfo: ConnectionInfo, tableName: String, data: List<Map<String, String?>>)
}