package com.ataccama.dbeaver.service

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.event.EventListener
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils
import java.sql.DriverManager
import java.util.stream.IntStream

private val LOGGER = LoggerFactory.getLogger(DatabaseTestSupport::class.java)

class MySqlHolder : DatabaseTestSupport {

    private var mySqlContainers: MutableMap<ConnectionInfo, MySQLContainer<Nothing>> = mutableMapOf()

    @EventListener
    fun init(event: InitTestEvent) {
        mySqlContainers.clear()
    }

    override fun createDatabase(): ConnectionInfo {
        val databaseName = RandomStringUtils.randomAlphabetic(5)
        val username = RandomStringUtils.randomAlphabetic(10)
        val password = RandomStringUtils.randomAlphabetic(20)

        val mySqlContainer = with(MySQLContainer<Nothing>("mysql:8.0.22")) {
            withDatabaseName(databaseName)
            withUsername(username)
            withPassword(password)
            withExposedPorts(3306)
            withEnv("MYSQL_ROOT_HOST", "%")
            this
        }

        mySqlContainer.start()

        val connectionInfo = ConnectionInfo(
                hostName = "localhost",
                databaseName = databaseName,
                port = mySqlContainer.getMappedPort(3306),
                username = username,
                password = password
        )

        mySqlContainers[connectionInfo] = mySqlContainer

        return connectionInfo
    }

    @EventListener
    fun tearDown(event: TearDownTestEvent) {
        mySqlContainers.values.forEach {
            if (it.isRunning) {
                try {
                    it.stop()
                } catch (ex: Exception) {
                    LOGGER.error("container ${it.containerName} cannot be stopped", ex)
                }
            }
        }
    }

    override fun createSchema(connectionInfo: ConnectionInfo, schema: String) {
        executeSql(connectionInfo, "CREATE SCHEMA $schema")
        executeSql(connectionInfo, "GRANT ALL PRIVILEGES ON $schema.* TO '${connectionInfo.username}'@'%'")
    }

    override fun createTable(connectionInfo: ConnectionInfo, tableName: String, columns: List<ColumnDefinition>) {
        executeSql(connectionInfo, "CREATE TABLE $tableName (${columns.joinToString(",", transform = ColumnDefinition::toSql)})")
    }

    override fun insertData(connectionInfo: ConnectionInfo, tableName: String, data: List<Map<String, String?>>) {
        data.forEach { insertRow(connectionInfo, tableName, it) }
    }

    private fun insertRow(connectionInfo: ConnectionInfo, tableName: String, data: Map<String, String?>) {
        executeSql(connectionInfo, "INSERT INTO $tableName values(${data.values.joinToString(",") { "?" }})", data.values.toList())
    }

    private fun executeSql(connectionInfo: ConnectionInfo, sql: String, params: List<Any?> = listOf()) {
        val connection = DriverManager.getConnection(mySqlContainers[connectionInfo]!!.jdbcUrl, "root", connectionInfo.password)

        connection.use {
            it.autoCommit = true
            it.prepareStatement(sql).use { statement ->
                IntStream.range(0, params.size).forEach { index ->
                    statement.setObject(index + 1, params[index])
                }
                statement.execute()
            }
        }
    }
}

@ConditionalOnExpression("\${com.atacama.dbeaver.ittest:false}")
@Configuration
class IntegrationTestConfig {

    @Bean
    @Primary
    fun databaseHolder() = MySqlHolder()
}