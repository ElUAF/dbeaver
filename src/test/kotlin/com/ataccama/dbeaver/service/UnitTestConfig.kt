package com.ataccama.dbeaver.service

import arrow.core.Either
import com.ataccama.dbeaver.service.dto.ColumnMetadataDto
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.problems.DbOperationConnectionFailedProblem
import com.ataccama.dbeaver.service.problems.DbOperationSqlProblem
import com.ataccama.dbeaver.service.problems.Problem
import com.ataccama.dbeaver.service.repository.DatabaseInfoRepository
import com.ataccama.dbeaver.service.repository.DbConnectionRepository
import com.ataccama.dbeaver.service.service.DatabaseInfoRepositoryAccessor
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.function.Executable
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.event.EventListener
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils
import java.util.*

class DbConnectionRepositoryStub : DbConnectionRepository {

    private val entities: MutableMap<Long, DbConnectionEntity> = mutableMapOf()

    private var sequence: Long = 0

    override fun <S : DbConnectionEntity?> save(entity: S): S = if (entity?.id == 0L) {
        val newEntity = entity.copy(id = ++sequence)
        entities[newEntity.id] = newEntity
        newEntity as S
    } else {
        entities.put(entity?.id ?: throw IllegalStateException("id cannot be null"), entity)
        entity
    }

    override fun findAll(): MutableIterable<DbConnectionEntity> = entities.values

    override fun deleteAll(entities: MutableIterable<DbConnectionEntity>) = entities.forEach { deleteById(it.id) }

    override fun deleteAll() = entities.clear()

    override fun <S : DbConnectionEntity?> saveAll(entities: MutableIterable<S>): List<S> = entities.map { save(it) }

    override fun count(): Long = entities.values.size.toLong()

    override fun findAllById(ids: MutableIterable<Long>): MutableIterable<DbConnectionEntity> = entities.values
            .filter { ids.contains(it.id) }
            .toMutableList()


    override fun delete(entity: DbConnectionEntity) {
        deleteById(entity.id)
    }

    override fun findById(id: Long): Optional<DbConnectionEntity> = Optional.ofNullable(entities[id])

    override fun existsById(id: Long): Boolean = entities[id] != null

    override fun deleteById(id: Long) {
        entities.remove(id)
    }

    @EventListener
    fun init(event: InitTestEvent) {
        deleteAll()
    }
}

data class Table(
        val metadata: List<ColumnDefinition>,
        val data: MutableList<Map<String, String?>> = mutableListOf()
) {
    fun insertData(newData: List<Map<String, String?>>) {
        Assertions.assertAll(
                newData.stream()
                        .map { row ->
                            Executable {
                                Assertions.assertEquals(
                                        metadata.size,
                                        row.size,
                                        "given data has different size than table metadata; metadata [$metadata]; data [$row]"
                                )
                            }
                        }
        )

        data.addAll(newData)
    }
}

class TestMySqlDatabaseInfoRepository(connectionInfo: ConnectionInfo) : DatabaseInfoRepository {

    private val schemas: MutableList<String> = mutableListOf("information_schema", connectionInfo.databaseName)
    private val tables: MutableMap<String, Table> = mutableMapOf()

    override fun findSchemas(): Either<Problem, List<String>> = Either.right(schemas)

    override fun findTables(): Either<Problem, List<String>> = Either.right(tables.keys.toList())

    override fun findTableStatistics(tableName: String): Either<Problem, TableStatisticsDto> = withTable(tableName) {
        TableStatisticsDto(
                columnsCount = it.metadata.size,
                recordCount = it.data.size.toLong()
        )
    }

    override fun findTableColumns(tableName: String): Either<Problem, List<ColumnMetadataDto>> = withTable(tableName) {
        it.metadata.map { it.toMetadataDto() }
    }

    override fun findTableData(tableName: String): Either<Problem, List<Map<String, String?>>> = withTable(tableName) {
        it.data
    }

    override fun findTableColumnsStatistics(tableName: String, columnName: String): Either<Problem, ColumnStatisticsDto> = withTable(tableName) {
        val columnData: List<Double> = it.data
                .map { row -> row.entries.first { column -> column.key == columnName } }
                .filter { row -> row.value != null }
                .map { row -> row.value!!.toDouble() }

        ColumnStatisticsDto(
                min = columnData.minOrNull(),
                max = columnData.maxOrNull(),
                avg = columnData.average(),
                median = if (columnData.isNotEmpty()) columnData[columnData.size / 2] else null
        )
    }

    private fun <K> withTable(tableName: String, action: (table: Table) -> K): Either<Problem, K> {
        val table = tables[tableName] ?: return Either.left(DbOperationSqlProblem("table $tableName is not found"))
        return Either.right(action(table))
    }

    fun createSchema(schema: String) {
        schemas.add(schema)
    }

    fun createTable(name: String, columns: List<ColumnDefinition>) {
        tables[name] = Table(columns)
    }

    fun insertData(tableName: String, data: List<Map<String, String?>>) {
        tables[tableName]?.insertData(data)
    }
}

class TestDatabaseInfoRepositoryAccessor : DatabaseInfoRepositoryAccessor, DatabaseTestSupport {

    private val databases: MutableMap<ConnectionInfo, TestMySqlDatabaseInfoRepository> = mutableMapOf()

    override fun <K> useRepository(dbConnection: DbConnectionEntity, operation: (repository: DatabaseInfoRepository) -> Either<Problem, K>): Either<Problem, K> {
        val repository = databases[dbConnection.toConnectionInfo()] ?: return Either.left(DbOperationConnectionFailedProblem(dbConnection.id))
        return operation(repository)
    }

    @EventListener
    fun init(event: InitTestEvent) {
        databases.clear()
    }

    override fun createDatabase(): ConnectionInfo {
        val connectionInfo = ConnectionInfo(
                hostName = RandomStringUtils.randomAlphabetic(10),
                port = RandomStringUtils.randomNumeric(3).toInt(),
                databaseName = RandomStringUtils.randomAlphabetic(5),
                username = RandomStringUtils.randomAlphabetic(10),
                password = RandomStringUtils.randomAlphabetic(20)
        )
        databases[connectionInfo] = TestMySqlDatabaseInfoRepository(connectionInfo)
        return connectionInfo
    }

    override fun createSchema(connectionInfo: ConnectionInfo, schema: String) {
        databases[connectionInfo]?.createSchema(schema)
    }

    override fun createTable(connectionInfo: ConnectionInfo, tableName: String, columns: List<ColumnDefinition>) {
        databases[connectionInfo]?.createTable(tableName, columns)
    }

    override fun insertData(connectionInfo: ConnectionInfo, tableName: String, data: List<Map<String, String?>>) {
        databases[connectionInfo]?.insertData(tableName, data)
    }
}

@ConditionalOnExpression("\${com.atacama.dbeaver.unittest:false}")
@Configuration
class UnitTestConfig {

    @Bean
    fun repository(): DbConnectionRepository = DbConnectionRepositoryStub()

    @Bean
    @Primary
    fun dbAccessor(): DatabaseInfoRepositoryAccessor = TestDatabaseInfoRepositoryAccessor()
}