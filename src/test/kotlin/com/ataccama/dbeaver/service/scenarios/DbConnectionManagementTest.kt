package com.ataccama.dbeaver.service.scenarios

import com.ataccama.dbeaver.service.DBeaverTestBase
import com.ataccama.dbeaver.service.entity.DbConnectionEntity
import com.ataccama.dbeaver.service.problems.Problem
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils
import java.util.stream.Stream

class DbConnectionManagementTest : DBeaverTestBase() {

    @Test
    fun `new database connection can be created`() {
        val dbConnection = `when valid MySQL database connection is created`()
        Assert.assertTrue("", dbConnection.id > 0)
    }


    @ParameterizedTest
    @MethodSource("wrongDbConnectionEntities")
    fun `new database connection with wrong data cannot be created`(wrongDbEntity: DbConnectionEntity) {
        val result = `when MySQL database connection is created`(wrongDbEntity)

        `then expect validation problem`(result)
    }

    @Test
    fun `database connection can be updated`() {
        val dbConnection = `given there is valid database connection`()

        val actual = `when database connection is updated`(
                entity = dbConnection,
                name = UPDATED_NAME,
                databaseName = UPDATED_DATABASE,
                port = UPDATED_PORT,
                username = UPDATED_USERNAME,
                password = UPDATED_PASSWORD
        )

        val expected = getExpectedEntity(entity = dbConnection,
                name = UPDATED_NAME,
                databaseName = UPDATED_DATABASE,
                port = UPDATED_PORT,
                username = UPDATED_USERNAME,
                password = UPDATED_PASSWORD)

        actual.fold(
                { Assertions.fail<Problem>("update should return success but was $it") },
                { Assertions.assertEquals(expected, it) }
        )
    }

    @ParameterizedTest
    @MethodSource("wrongDbConnectionEntities")
    fun `database connection cannot be updated with wrong data`(wrongDbEntity: DbConnectionEntity) {
        val dbConnection = `given there is valid database connection`()

        val result = `when database connection is updated`(dbConnection, wrongDbEntity)

        `then expect validation problem`(result)
    }

    @Test
    fun `update not existing entity`() {
        val actual = `when database connection is updated`(
                entity = DEFAULT_DB_CONNECTION
        )

        `then db connection is not found`(actual)
    }

    @Test
    fun `listing empty database`() {
        val expected = listOf<DbConnectionEntity>()

        `then there is database connections`(expected)
    }

    @Test
    fun `listing one item in database`() {
        val dbConnection = `given there is valid database connection`()

        val expected = listOf(dbConnection)

        `then there is database connections`(expected)
    }

    @Test
    fun `deleted item is not found`() {
        val dbConnection = `given there is valid database connection`()

        `when database connection is deleted`(dbConnection)

        `then there is database connections`(listOf())
    }

    @Test
    fun `delete item is not found but another one does`() {
        val dbConnectionToBeDeleted = `given there is valid database connection`()
        val existingDbConnection = `given there is valid database connection`()

        `when database connection is deleted`(dbConnectionToBeDeleted)

        `then there is database connections`(listOf(existingDbConnection))
    }

    companion object {
        const val UPDATED_NAME = "UPDATED_NAME"
        const val UPDATED_DATABASE = "UPDATED_DATABASE"
        const val UPDATED_PORT = DEFAULT_DB_CONNECTION_PORT + 1
        const val UPDATED_USERNAME = "UPDATED_USERNAME"
        const val UPDATED_PASSWORD = "UPDATED_PASSWORD"

        @JvmStatic
        fun wrongDbConnectionEntities(): Stream<Arguments> = Stream.of(
                Arguments.of(DEFAULT_DB_CONNECTION.copy(name = "")),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(name = RandomStringUtils.randomAlphabetic(1000))),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(hostName = "")),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(hostName = RandomStringUtils.randomAlphabetic(1000))),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(databaseName = "")),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(databaseName = RandomStringUtils.randomAlphabetic(1000))),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(port = 100000)),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(port = -1)),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(username = "")),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(username = RandomStringUtils.randomAlphabetic(1000))),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(password = "")),
                Arguments.of(DEFAULT_DB_CONNECTION.copy(password = RandomStringUtils.randomAlphabetic(1000)))
        )
    }
}