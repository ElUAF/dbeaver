package com.ataccama.dbeaver.service.scenarios

import com.ataccama.dbeaver.service.ColumnDefinition
import com.ataccama.dbeaver.service.DBeaverTestBase
import com.ataccama.dbeaver.service.dto.ColumnStatisticsDto
import com.ataccama.dbeaver.service.dto.TableStatisticsDto
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

class DbInfoTest : DBeaverTestBase() {

    @Test
    fun `empty database returns empty schemas and tables`() {
        val dbConnection = `given there is valid database up, running and stored`()

        Assertions.assertAll(
                Executable { `then schemes should be default and rest is equals to`(dbConnection, listOf()) },
                Executable { `then tables should be`(dbConnection, listOf()) }
        )
    }

    @Test
    fun `database with sample data returns with new scheme, new table, table metadata and table data`() {
        val schema = "test_schema_1"
        val table = "test_table_1"

        val id = "id"
        val name = "name"

        val columnDefinition = listOf(
                ColumnDefinition(
                        name = id,
                        type = "int",
                        primaryKey = true,
                        nullable = false
                ),
                ColumnDefinition(
                        name = name,
                        type = "char(35)"
                )
        )

        val tableData = listOf(
                mapOf(
                        id to "1",
                        name to "name1"
                ),
                mapOf(
                        id to "2",
                        name to "name2"
                ),
                mapOf(
                        id to "9",
                        name to "name9"
                )
        )

        val dbConnection = `given there is valid database up, running and stored`()
        `given there is schema`(dbConnection, schema)
        `given there is table`(dbConnection, table, columnDefinition)
        `given table has following data`(dbConnection, table, tableData)

        Assertions.assertAll(
                Executable { `then schemes should be default and rest is equals to`(dbConnection, listOf(schema)) },
                Executable { `then tables should be`(dbConnection, listOf(table)) },
                Executable { `then table should have metadata`(dbConnection, table, columnDefinition) },
                Executable { `then table should have data`(dbConnection, table, tableData) },
                Executable { `then table should have expected statistics`(dbConnection, table, TableStatisticsDto(recordCount = tableData.size.toLong(), columnsCount = columnDefinition.size)) },
                Executable { `then column should have expected statistics`(dbConnection, table, id, ColumnStatisticsDto(max = 9.0, min = 1.0, avg = 4.0, median = 2.0)) }
        )
    }
}